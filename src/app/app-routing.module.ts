import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PhotosComponent } from './pages/photos/photos.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: "full",
    redirectTo: 'dashboard'
  },
  {
    path: 'dashboard',
    loadChildren: () => import('../app/pages/dashboard/dashboard.module').then(module => module.DashboardModule)
  },
  {
    path: 'posts',
    loadChildren: () => import('../app/pages/posts/posts.module').then(module => module.PostsModule)
  }
  ,
  {
    path: 'albums',
    loadChildren: () => import('../app/pages/albums/albums.module').then(module => module.AlbumsModule)
  },
  {
    path: 'photos',
    loadChildren: () => import('../app/pages/photos/photos.module').then(module => module.PhotosModule)
  },
  {
    path: 'users',
    loadChildren: () => import('../app/pages/user/user.module').then(module => module.UserModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
