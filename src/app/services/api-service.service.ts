import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Albums, AlbumsWithPhotos, Photos, Posts } from 'src/Models/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  /* This service is being use to make all major api calls and caching the response in private Behaviour subjects and then Observable are beign derived out of these BehaviorSubject accordinly, these Observeables can be subscribed in any component to get data instantaneously and we don't have make api calss every time we go a page */
  private postsSubject = new BehaviorSubject<Posts[]>([]);
  getPosts$: Observable<Posts[]> = this.postsSubject.asObservable();

  private photosSubject = new BehaviorSubject<Photos[]>([]);
  getPhotos$: Observable<Photos[]> = this.photosSubject.asObservable();

  private albumsSubject = new BehaviorSubject<Albums[]>([]);
  getAlbums$: Observable<Albums[]> = this.albumsSubject.asObservable();

  private albumsWithPhotosSubject = new BehaviorSubject<AlbumsWithPhotos[]>([]);
  getAlbumsWithPhotos$: Observable<AlbumsWithPhotos[]> = this.albumsWithPhotosSubject.asObservable();

  constructor(private httpClient: HttpClient) {
    this.getPosts();
    this.getAlbums();
  }
  private getPosts() {
    this.httpClient.get<Posts[]>('https://jsonplaceholder.typicode.com/posts').subscribe(posts => {
      this.postsSubject.next(posts);
    })
  }
  private getAlbums() {
    this.httpClient.get<Albums[]>('https://jsonplaceholder.typicode.com/albums').subscribe(albums => {
      this.albumsSubject.next(albums);
      this.getPhotos();
    })
  }
  /* In this method we are getting all the photos and placing them in the there respective albums so that it will be easy to show these photos inside albums page and then a single page (/albums & /albums/1); at last we are savig them in BehaviorSubject so that it's respective Observeable can be subscribed to get this modified data*/
  private getPhotos() {
    this.httpClient.get<Photos[]>('https://jsonplaceholder.typicode.com/photos').subscribe(photos => {
      this.photosSubject.next(photos);
      let albums: Albums[] = this.albumsSubject.getValue();
      let albumsWithPhotos: AlbumsWithPhotos[] = [];
      albums.forEach(album => {
        let modifiedAlbum: AlbumsWithPhotos;
        let extractedPhotos: Photos[];
        extractedPhotos = photos.filter(photo => photo.albumId == album.id);
        modifiedAlbum = { ...album, photos: extractedPhotos }
        albumsWithPhotos.push(modifiedAlbum);
      })
      this.albumsWithPhotosSubject.next(albumsWithPhotos)
    })
  }

}
