import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Albums, Photos, Posts } from 'src/Models/interfaces';

import { ApiService } from './api-service.service';

describe('ApiServiceService', () => {
  let service: ApiService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  beforeEach(() => {
    const mockPosts: Posts[] = [{ body: 'Test description', id: 1, title: 'Test Heading', userId: 1 }]
    const mockAlbums: Albums[] = [{ id: 1, title: 'Test Heading', userId: 1 }]
    const mockPhotos: Photos[] = [{ id: 1, title: 'Test Heading', albumId: 1, thumbnailUrl: 'test', url: 'test' }]
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule], providers: [ApiService] });
    service = TestBed.inject(ApiService);

    /* httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController)
    const postReq = httpTestingController.expectOne('https://jsonplaceholder.typicode.com/posts')
    expect(postReq.request.method).toBe('GET')
    const albumsReq = httpTestingController.expectOne('https://jsonplaceholder.typicode.com/albums')
    expect(albumsReq.request.method).toBe('GET')
    const photosReq = httpTestingController.expectOne('https://jsonplaceholder.typicode.com/photos')
    expect(photosReq.request.method).toBe('GET')

    postReq.flush(mockPosts)
    albumsReq.flush(mockAlbums)
    photosReq.flush(mockPhotos) */

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
