import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  /* Being used to save the current page number of the pages with pagination so user can come back to same page after viewing album,photo or post */
  postsPage: number = 1;
  albumsPage: number = 1;
  photosPage: number = 1;
  constructor() { }
  /* This method is returning a  whole number for total pages to show on top of all pagination pages For Example (Page 1 of 9) */
  getTotalPagesToShow(totalItems: number, itemsPerPage: number): number {
    let totalPages = 1
    let total = totalItems / itemsPerPage
    let roundUpTotal = (Math.ceil((total) * 10) / 10).toFixed(0);
    if ((total) > (+roundUpTotal)) {
      totalPages = (+roundUpTotal) + 1;
    } else {
      totalPages = Number(roundUpTotal)
    }
    return totalPages;
  }
}
