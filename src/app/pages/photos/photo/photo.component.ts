import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {
  selectedPhot: any;
  id: number = 0
  constructor(private route: ActivatedRoute, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      let id = params.get('id');
      if (id) {
        this.id = +id;
      }
      this.getPicData();
    })

  }
  getPicData() {
    this.httpClient.get(`https://jsonplaceholder.typicode.com/photos/${this.id}`).subscribe(data => {
      this.selectedPhot = data;
    })
  }
}
