import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';
import { AlbumsWithPhotos, Photos } from 'src/Models/interfaces';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss']
})
export class AlbumComponent implements OnInit, OnDestroy {
  selectedAlbum: AlbumsWithPhotos | any;
  selectedAlbumPhotosBackup: any[] = [];
  albumId: number = 0;
  albumSub: any;
  constructor(private apiSerivce: ApiService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    document.body.scrollTop = 0;
    this.route.paramMap.subscribe(params => {
      let id = params.get('id');
      if (id) {
        this.albumId = +id;
        this.albumSub = this.apiSerivce.getAlbumsWithPhotos$.subscribe(albums => {
          if (!albums.length) {
            return;
          }
          this.selectedAlbum = albums.find(album => album.id == this.albumId);
          this.selectedAlbumPhotosBackup = this.selectedAlbum.photos
        })
      }
    })

  }
  search($event: NgModel) {
    let searchString: string = $event.value.toLowerCase();
    if (!searchString) {
      this.router.navigate([], {
        queryParams: {
          search: null
        }
      });
      return
    }
    this.router.navigate([], {
      queryParams: {
        search: searchString
      },
      // preserve the existing query params in the route
      queryParamsHandling: 'merge',
    });
    /* Filtering posts according to seaerch string */
    this.selectedAlbum.photos = this.selectedAlbumPhotosBackup.filter((photo: Photos) => photo.title.toLowerCase().includes(searchString) || photo.id.toString().includes(searchString) || photo.url.toLowerCase().includes(searchString));
  }
  ngOnDestroy() {
    this.albumSub.unsubscribe()
  }
}
