import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';
import { CommonService } from 'src/app/services/common.service';
import { Photos } from 'src/Models/interfaces';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit, OnDestroy {
  photos: Photos[] = [];
  photosBackup: Photos[] = [];
  searchString: string = '';
  sortingValue: string = 'asending';
  /* spinner */
  showSpinner: boolean = true;
  /* Pagination */
  photosOnOnePage: number = 50;
  page: number = 1;
  totalPagesToShow: number = 1;
  totalItems: number = 0;
  /* Subscription */
  photosSub: any;
  constructor(private apiService: ApiService, private commonService: CommonService, private router: Router) { }

  ngOnInit(): void {
    document.body.scrollTop = 0;/* To reset page to the top */
    /* Saving page state to a service and getting it agian so that user can always get back to the page he was visiting current page functionality */
    this.page = this.commonService.photosPage

    this.photosSub = this.apiService.getPhotos$.subscribe(photos => {
      if (!photos.length) {
        return;
      }
      this.showSpinner = false;
      this.photos = photos
      this.photosBackup = photos
      this.totalItems = this.photos.length;
      this.totalPagesToShow = this.commonService.getTotalPagesToShow(this.totalItems, this.photosOnOnePage);

    })
    this.sort();
  }
  pageChanged($event: number) {
    this.page = $event
    this.commonService.photosPage = this.page;
  }
  search($event: NgModel) {
    let searchString: string = $event.value.toLowerCase();
    if (!searchString) {
      this.router.navigate([], {
        queryParams: {
          search: null
        }
      });
      return
    }
    this.setQuerryParams();
    /* Filtering posts according to seaerch string */
    this.photos = this.photosBackup.filter(photo => photo.title.toLowerCase().includes(searchString) || photo.url.toLowerCase().includes(searchString) || (photo.id.toString()).includes(searchString));
    /* Resetting pagination*/
    this.totalItems = this.photos.length;
    this.totalPagesToShow = this.commonService.getTotalPagesToShow(this.totalItems, this.photosOnOnePage);
    this.page = 1;
    this.sort();
  }
  sort() {
    if (this.sortingValue == 'asending') {
      this.photos.sort((a, b) => a.id - b.id);
      this.setQuerryParams()
      return;
    }
    this.photos.sort((a, b) => b.id - a.id);
    this.setQuerryParams();
  }
  setQuerryParams() {
    this.router.navigate([], {
      queryParams: {
        search: this.searchString.length ? this.searchString : null,
        sort: this.sortingValue
      },
      // preserve the existing query params in the route
      queryParamsHandling: 'merge',
    });
  }
  ngOnDestroy() {
    this.photosSub.unsubscribe();
  }
}
