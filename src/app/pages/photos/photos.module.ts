import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotosRoutingModule } from './photos-routing.module';
import { PhotosComponent } from './photos.component';
import { PhotoComponent } from './photo/photo.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [PhotosComponent, PhotoComponent],
  imports: [
    CommonModule,
    PhotosRoutingModule,
    NgxPaginationModule,
    FormsModule
  ]
})
export class PhotosModule { }
