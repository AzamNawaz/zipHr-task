import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';
import { CommonService } from 'src/app/services/common.service';
import { Posts } from 'src/Models/interfaces';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {
  posts: Posts[] = [];
  postsBackup: Posts[] = [];
  sortingValue: string = 'asending'
  searchString: string = ''
  /* Spinner */
  showSpinner: boolean = true;
  /* Pagination */
  totalItems: number = 0;
  postsOnOnePage: number = 5;
  totalPagesToShow: number = 1;
  page: number = 0
  /* Subscriptions */
  postsSub: any;
  constructor(
    private router: Router, private apiService: ApiService, private commonService: CommonService) { }

  ngOnInit(): void {
    this.getPosts();
    /* Saving page state to a service and getting it agian so that user can always get back to the page he was visiting current page functionality */
    this.page = this.commonService.postsPage;
  }
  getPosts() {
    this.postsSub = this.apiService.getPosts$.subscribe(posts => {
      if (!posts.length) {
        return;
      }
      this.showSpinner = false;
      this.posts = posts;
      this.postsBackup = posts;
      /* Pagination */
      this.totalItems = this.posts.length;
      this.totalPagesToShow = this.commonService.getTotalPagesToShow(this.totalItems, this.postsOnOnePage);
    })
  }
  pageChanged($event: number) {
    this.page = $event;
    this.commonService.postsPage = this.page;
  }
  navigateToPost(post: any) {
    this.router.navigateByUrl('/posts/' + post.id);
  }
  search($event: NgModel) {
    let searchString: string = $event.value.toLowerCase();
    if (!searchString) {
      this.router.navigate([], {
        queryParams: {
          search: null
        }
      });
      return
    }
    this.router.navigate([], {
      queryParams: {
        search: searchString
      },
      // preserve the existing query params in the route
      queryParamsHandling: 'merge',
    });
    /* Filtering posts according to seaerch string */
    this.posts = this.postsBackup.filter(post => post.title.toLowerCase().includes(searchString) || post.body.toLowerCase().includes(searchString) || ('user ' + post.userId.toString()).includes(searchString));
    /* Resetting pagination*/
    this.totalItems = this.posts.length;
    this.totalPagesToShow = this.commonService.getTotalPagesToShow(this.totalItems, this.postsOnOnePage);
    this.page = 1;
    this.sort();
  }
  sort() {
    if (!this.searchString.length) {
      this.posts = [];
      this.postsBackup.forEach(post => {
        this.posts.push(post);
      })
    }
    if (this.sortingValue == 'asending') {
      return;
    }
    if (this.sortingValue == 'desending') {
      this.posts.reverse();
      return;
    }
    if (this.sortingValue == 'userAsending') {
      this.posts.sort((a, b) => a.id - b.id)
      return;
    }
    if (this.sortingValue == 'userDesending') {
      this.posts.sort((a, b) => b.id - a.id)
      return;
    }

  }
  ngOnDestroy() {
    this.postsSub.unsubscribe();
  }
}
