import { TestBed } from '@angular/core/testing';

import { CommonService } from './common.service';

describe('CommonService', () => {
  let service: CommonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CommonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should return a whole number', () => {
    const pages = service.getTotalPagesToShow(121, 10)
    expect(pages).toBe(13);
    const page = service.getTotalPagesToShow(100,10)
    expect(page).toBe(10);
  });
});
