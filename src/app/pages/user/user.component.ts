import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';
import { Albums, Posts, User } from 'src/Models/interfaces';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  userId: number = 0;
  user: User | any;
  posts: Posts[] = [];
  albums: Albums[] = [];
  /* Subscriptions */
  postsSub: any;
  albumsSub: any;
  constructor(private httpClient: HttpClient, private route: ActivatedRoute, private apiService: ApiService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      let id = params.get('id')
      if (id) {
        this.userId = +id
        this.getUserData();
        this.getUserPosts();
        this.getUserAlbums();
      }
    })
  }
  getUserPosts() {
    this.postsSub = this.apiService.getPosts$.subscribe(posts => {
      this.posts = posts.filter(post => post.userId == this.userId)
    })
  }
  getUserAlbums() {
    this.albumsSub = this.apiService.getAlbums$.subscribe(albums => {
      this.albums = albums.filter(album => album.userId == this.userId);
    })
  }
  getUserData() {
    this.httpClient.get<User>(`https://jsonplaceholder.typicode.com/users/${this.userId}`).subscribe(user => {
      this.user = user
    })
  }
  ngOnDestroy() {
    this.postsSub.unsubscribe();
    this.albumsSub.unsubscribe();
  }
}
