import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';
import { CommonService } from 'src/app/services/common.service';
import { AlbumsWithPhotos } from 'src/Models/interfaces';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit, OnDestroy {
  albums: AlbumsWithPhotos[] = [];
  albumsBackup: AlbumsWithPhotos[] = [];
  searchString: string = '';
  sortingValue: string = 'asending'
  /* Spinner */
  showSpinner: boolean = true;
  /* Pagination */
  page: number = 1;
  AlbumsOnOnePage: number = 12;
  totalItems: number = 0;
  totalPagesToShow: number = 0;
  /* Subscription */
  photosSub: any;
  constructor(private apiService: ApiService, private commonService: CommonService, private router: Router) { }

  ngOnInit(): void {
    document.body.scrollTop = 0;
    /* Saving page state to a service and getting it agian so that user can always get back to the page he was visiting current page functionality */
    this.page = this.commonService.albumsPage;
    this.getAlbums();
    this.sort()
  }
  getAlbums() {
    this.photosSub = this.apiService.getAlbumsWithPhotos$.subscribe(albums => {
      if (!albums.length) {
        return
      }
      this.showSpinner = false;
      this.albums = albums
      this.albumsBackup = albums
      this.totalItems = this.albums.length
      this.totalPagesToShow = this.commonService.getTotalPagesToShow(this.totalItems, this.AlbumsOnOnePage);

    })
  }
  pageChanged($event: number) {
    this.page = $event;
    this.commonService.albumsPage = this.page;
  }
  search($event: NgModel) {
    let searchString: string = $event.value.toLowerCase();
    if (!searchString) {
      this.router.navigate([], {
        queryParams: {
          search: null,
          sort: this.sortingValue
        },
      });
      return
    }
    this.setQuerryParams();
    /* Filtering posts according to seaerch string */
    this.albums = this.albumsBackup.filter(album => album.title.toLowerCase().includes(searchString) || album.id.toString().includes(searchString) || ('user ' + album.userId.toString()).includes(searchString));
    /* Resetting pagination*/
    this.totalItems = this.albums.length;
    this.totalPagesToShow = this.commonService.getTotalPagesToShow(this.totalItems, this.AlbumsOnOnePage);
    this.page = 1
    this.sort();
  }
  sort() {
    if (!this.searchString.length) {
      this.albums = [];
      this.albumsBackup.forEach(album => {
        this.albums.push(album);
      })
    }
    if (this.sortingValue == 'asending') {
      this.setQuerryParams();
      return;
    }
    if (this.sortingValue == 'desending') {
      this.albums.reverse();
      this.setQuerryParams();
      return;
    }
    if (this.sortingValue == 'userAsending') {
      this.albums.sort((a, b) => a.id - b.id)
      this.setQuerryParams();
      return;
    }
    if (this.sortingValue == 'userDesending') {
      this.albums.sort((a, b) => b.id - a.id)
      this.setQuerryParams();
      return;
    }

  }
  setQuerryParams() {
    this.router.navigate([], {
      queryParams: {
        search: this.searchString.length ? this.searchString : null,
        sort: this.sortingValue
      },
      // preserve the existing query params in the route
      queryParamsHandling: 'merge',
    });
  }
  ngOnDestroy() {
    this.photosSub.unsubscribe()
  }

}
