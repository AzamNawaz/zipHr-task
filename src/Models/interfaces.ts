export interface Posts {
    body: string,
    id: number,
    title: string,
    userId: number
}
export interface Photos {
    albumId: number,
    id: number,
    thumbnailUrl: string,
    title: string,
    url: string
}
export interface Albums {
    id: number,
    title: string,
    userId: number
}
export interface AlbumsWithPhotos {
    id: number,
    title: string,
    userId: number,
    photos: Photos[];
}
export interface User {
    email: string,
    id: number
    name: string,
    phone: string,
    username: string,
    website: string,
    address: Address,
    company: Company
}
interface Address {
    street: string,
    city: string,
    suite: string,
    zipcode: string,
    geo: {
        lat: string,
        lng: string
    }
}
interface Company {
    bs: string,
    catchPhrase: string,
    name: string
}