import { Component, OnDestroy, OnInit } from '@angular/core';
import { Albums, Photos, Posts } from 'src/Models/interfaces';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  posts: Posts[] = [];
  photos: Photos[] = [];
  albums: Albums[] = []
  /* Pagination */
  page: number = 0;
  photosOnOnePage: number = 50;
  totalPhotos: number = 0;
  /* Subscriptions */
  postsSub: any;
  photosSub: any;
  albumsSub: any;
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    document.body.scrollTop = 0
    this.getPosts();
    this.getPhotos();
    this.getAlbums();
  }
  getPosts() {
    this.postsSub = this.apiService.getPosts$.subscribe(res => {
      this.posts = res
    })
  }
  getPhotos() {
    this.photosSub = this.apiService.getPhotos$.subscribe(res => {
      this.photos = res
      this.totalPhotos = this.photos.length
    })
  }
  getAlbums() {
    this.albumsSub = this.apiService.getAlbums$.subscribe(res => {
      this.albums = res
    })
  }
  pageChanged($event: number) {
    this.page = $event;
  }
  ngOnDestroy() {
    this.postsSub.unsubscribe()
    this.photosSub.unsubscribe()
    this.albumsSub.unsubscribe()
  }
}
