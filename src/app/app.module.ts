import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardModule } from "../app/pages/dashboard/dashboard.module";
import { PostsModule } from './pages/posts/posts.module';
import { AlbumsModule } from './pages/albums/albums.module';
import { PhotosModule } from './pages/photos/photos.module';
import { UserModule } from './pages/user/user.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DashboardModule,
    PostsModule,
    AlbumsModule,
    PhotosModule,
    UserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
