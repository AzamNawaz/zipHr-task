import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api-service.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  selectedPost: any;
  id: number = 0;
  constructor(private apiService: ApiService, private route: ActivatedRoute, private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      let id = params.get('id')
      if (id) {
        this.id = +id;
      }
      /* Instead of calling API every time we initialize this page we can just use the cached response and extract the post ; but if posts length is too long we can simply call the API both implementations are provided; just comment out below snippet and uncomment the other it will work as same */
      this.apiService.getPosts$.subscribe(posts => {
        this.selectedPost = posts.find(post => post.id == this.id)
      })
      /* this.getPostData() */
    })
  }
  /*  getPostData() {
     this.httpClient.get<Posts>(`https://jsonplaceholder.typicode.com/posts/${this.id}`).subscribe(post => {
       this.selectedPost = post
     })
   } */
}
