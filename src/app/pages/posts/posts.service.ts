import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  page: number = 0;
  constructor(private httpClient: HttpClient) { }

}
